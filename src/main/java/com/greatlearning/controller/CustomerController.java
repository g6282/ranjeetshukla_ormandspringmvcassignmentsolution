package com.greatlearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.greatlearning.model.Customer;
import com.greatlearning.service.CustomerService;

@Controller
public class CustomerController {

	@Autowired
	CustomerService customerServicesImpl;

	@RequestMapping("/")
	public String customerList(Model model) {
		List<Customer> customers = customerServicesImpl.findAll();
		model.addAttribute("customers", customers);
		return "index";
	}

	@RequestMapping("/addCustomerForm")
	public String addCustomerForm(Model model) {
		Customer customer = new Customer();
		model.addAttribute("customer", customer);
		return "addCustomer";

	}

	@RequestMapping("/updateCustomerForm")
	public String updateCustomerForm(@RequestParam("id") int id, Model model) {
		Customer customer = customerServicesImpl.findById(id);
		model.addAttribute("customer", customer);
		return "addCustomer";

	}

	@PostMapping("/saveCustomer")
	public String saveCustomer(@RequestParam("id") int id, @RequestParam("fristName") String fristName,
			@RequestParam("lastName") String lastName, @RequestParam("email") String email) {

		Customer customer;
		if (id != 0) {
			customer = customerServicesImpl.findById(id);
			customer.setFristName(fristName);
			customer.setLastName(lastName);
			customer.setEmail(email);
		} else {
			customer = new Customer(fristName, lastName, email);
		}
		customerServicesImpl.save(customer);
		return "redirect:/";

	}

	@RequestMapping("/deleteCustomer")
	public String deleteCustomer(@RequestParam("id") int id, Model model) {
		customerServicesImpl.deleteById(id);
		return "redirect:/";
	}
}
