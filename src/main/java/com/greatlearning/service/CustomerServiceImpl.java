package com.greatlearning.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.dao.CustomerDao;
import com.greatlearning.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	CustomerDao customerDaoImpl;

	@Override
	public List<Customer> findAll() {
		return customerDaoImpl.findAll();
	}

	@Override
	public void save(Customer customer) {
		customerDaoImpl.save(customer);

	}

	@Override
	public void deleteById(int id) {
		customerDaoImpl.deleteById(id);

	}

	@Override
	public Customer findById(int id) {
		return customerDaoImpl.findById(id);
	}

}
