<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">

<title>Customer Relationship Management</title>
</head>
<body>
	<div class="container">
		<div
			style="background: #71d371; padding: 20px 10px; margin-bottom: 45px;">
			<h3>CUSTOMER RELATIONSHIP MANAGEMENT</h3>
		</div>

		<!-- Add a search form -->
		<form action="#" class="form-inline">
			<!-- Add a button -->
			<a href="/CRMCrud/addCustomerForm"
				class="btn btn-secondary btn-sm mb-3"> Add Customer </a>
		</form>
		<table class="table table-borderless table-striped">
			<thead>
				<tr style="background: #71d371">
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${customers}" var="customer">
					<tr>
						<td><c:out value="${customer.fristName}" /></td>
						<td><c:out value="${customer.lastName}" /></td>
						<td><c:out value="${customer.email}" /></td>
						<td><a
							href="/CRMCrud/updateCustomerForm?id=${customer.customer_id}">
								Update </a> | <a
							href="/CRMCrud/deleteCustomer?id=${customer.customer_id}"
							onclick="if (!(confirm('Are you sure you want to delete this Customer?'))) return false">
								Delete </a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>



