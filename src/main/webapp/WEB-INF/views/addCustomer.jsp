<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
	integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
	crossorigin="anonymous">

<title>Save Customer</title>
</head>

<body>

	<div class="container">
		<div
			style="background: #71d371; padding: 10px 10px; margin: 10px 0 45px;">
			<h3>CUSTOMER RELATIONSHIP MANAGEMENT</h3>
		</div>
		<p class="h4 mb-4">Save Customer</p>

		<form action="/CRMCrud/saveCustomer" method="POST">
			<div class="form-group row">
				<label for="fristName" class="col-sm-2 col-form-label">Frist
					Name : </label>
				<div class="col-sm-10">
					<input type="text" class="form-control-sm" id="fristName"
						name="fristName" placeholder="Enter your frist name"
						value="${customer.fristName}">
				</div>
			</div>

			<div class="form-group row">
				<label for="lastName" class="col-sm-2 col-form-label">Last
					Name : </label>
				<div class="col-sm-10">
					<input type="text" class="form-control-sm" id="lastName"
						name="lastName" placeholder="Enter your last name"
						value="${customer.lastName}">
				</div>
			</div>

			<div class="form-group row">
				<label for="email" class="col-sm-2 col-form-label">email : </label>
				<div class="col-sm-10">
					<input type="text" class="form-control-sm" id="email" name="email"
						placeholder="Enter your email" value="${customer.email}">
				</div>
			</div>


			<!-- Add hidden form field to handle update -->
			<input type="hidden" name="id" value="${customer.customer_id}" />
			<div class="form-group row">
				<label class="col-sm-2 col-form-label"></label>
				<div class="col-sm-10">
					<button type="submit" class="btn btn-secondary btn-sm col-2">Save</button>
				</div>
			</div>
		</form>

		<hr>
		<a href="/CRMCrud/">Back to List</a>

	</div>
</body>

</html>










